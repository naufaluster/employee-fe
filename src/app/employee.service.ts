import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { commonresponse } from './commonresponse';
import { Observable } from 'rxjs';
import { Employee } from './employee';
import { Position } from './position';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  getAllPosition(): Observable<commonresponse<Position[]>> {
    return this.http.get<commonresponse<Position[]>>("http://localhost:8080/position/list/all");
  }

  getEmployee(body): Observable<commonresponse<Employee[]>> {
    return this.http.get<commonresponse<Employee[]>>("http://localhost:8080/employee/list?idNumber=" + body.idNumber + "&name=" + body.name + "&limit=" + body.limit + "&offset=" + body.offset + "&orderBy=" + body.orderBy + "&sortType=" + body.sortType);
  }

  getEmployeeCount(body): Observable<commonresponse<Employee>> {
    return this.http.get<commonresponse<Employee>>("http://localhost:8080/employee/count?idNumber=" + body.idNumber + "&name=" + body.name + "&limit=" + body.limit + "&offset=" + body.offset);
  }

  registerEmployee(body): Observable<commonresponse<any>> {
    return this.http.post<commonresponse<any>>('http://localhost:8080/employee/registration', body);
  }

  updateEmployee(body): Observable<commonresponse<any>> {
    return this.http.post<commonresponse<any>>('http://localhost:8080/employee/update', body);
  }

  deleteEmployee(body): Observable<commonresponse<any>> {
    return this.http.post<commonresponse<any>>('http://localhost:8080/employee/deletion', body);
  }
}
