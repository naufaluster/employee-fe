import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  id: any = '';
  name: any = '';
  birthDate: any = '';
  positionId: any = '';
  idNumber: any = '';
  gender: any = '';

  position: any = [];

  genders = [1, 2];

  body: any = [];
  objective: any = '';
  data:any = [];

  constructor(private router: Router, private service: EmployeeService) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.body = this.router.getCurrentNavigation().extras.state;
      this.data = this.body.data;
      console.log(this.body.objective == 'update');
      if (this.body.objective == 'update') {
        this.objective = 'update';
        this.id = this.data.id;
        this.name = this.data.name;
        this.birthDate = this.data.birthDate;
        this.positionId = this.data.positionId;
        this.idNumber = this.data.idNumber;
        this.gender = this.data.gender;
      }
    }
  }

  ngOnInit(): void {
    this.getPosition();
  }

  back() {
    this.id = '';
    this.name = '';
    this.birthDate = '';
    this.positionId = '';
    this.idNumber = '';
    this.gender = '';
    this.router.navigate(['/index'])
  }

  message: any = [];
  checkMessage() {
    alert(this.message.join(',').replace(/,/g, '\n').split())
  }
  async save() {
    let body = {
      birthDate: this.birthDate,
      gender: this.gender,
      idNumber: this.idNumber,
      isDelete: 0,
      name: this.name,
      positionId: this.positionId
    }

    if (this.name.length < 1) {
      this.message.push('Nama tidak boleh kosong');
    }
    if (this.gender.length < 1) {
      this.message.push('Jenis kelamin tidak boleh kosong');
    }
    if (this.positionId.length < 1) {
      this.message.push('Posisi tidak boleh kosong');
    }
    if (this.idNumber.length < 1) {
      this.message.push('NIP tidak boleh kosong');
    }
    if (this.birthDate.length < 1) {
      this.message.push('Tanggal lahir tidak boleh kosong');
    }

    if (this.message.length > 0) {
      this.checkMessage();
    } else {
      let data = await this.service.registerEmployee(body).toPromise();
      if (data.code == 200) {
        this.back();
        alert(data.message);
      } else {
        alert(data.message);
      }
    }

  }

  async getPosition() {
    let data = await this.service.getAllPosition().toPromise();
    this.position = data.data;
  }

  async update(){
    let body = {
      birthDate: this.birthDate,
      gender: this.gender,
      idNumber: this.idNumber,
      isDelete: 0,
      name: this.name,
      positionId: this.positionId
    }

    if (this.name.length < 1) {
      this.message.push('Nama tidak boleh kosong');
    }
    if (this.gender.length < 1) {
      this.message.push('Jenis kelamin tidak boleh kosong');
    }
    if (this.positionId.length < 1) {
      this.message.push('Posisi tidak boleh kosong');
    }
    if (this.idNumber.length < 1) {
      this.message.push('NIP tidak boleh kosong');
    }
    if (this.birthDate.length < 1) {
      this.message.push('Tanggal lahir tidak boleh kosong');
    }

    if (this.message.length > 0) {
      this.checkMessage();
    } else {
      let data = await this.service.updateEmployee(body).toPromise();
      if (data.code == 200) {
        this.back();
        alert(data.message);
      } else {
        alert(data.message);
      }
    }
    
  }

  checkNumber() {
    const regex = /\D/;
    if (this.idNumber == regex) {
      this.idNumber = '';
    }

  }

}
