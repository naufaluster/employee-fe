export class Employee{
    id:number;
    name:string;
    birthDate:string;
    positionId:number;
    idNumber:number;
    gender:number;
    isDelete:number;

    constructor(id,name,birthDate,positionId,idNumber,gender,isDelete){
        this.id=id;
        this.name=name;
        this.birthDate=birthDate;
        this.positionId=positionId;
        this.idNumber=idNumber;
        this.gender=gender;
        this.isDelete=isDelete;
    }

}