export class Position{
    id:number;
    code:string;
    name:string;
    isDelete:number;

    constructor(id,code,name,isDelete){
        this.id=id;
        this.code=code;
        this.name=name;
        this.isDelete=isDelete;
    }

}