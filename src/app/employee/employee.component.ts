import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(
    private router: Router, private service: EmployeeService
  ) { }
  
  id:any = '';
  name: any='';
  birthDate: any='';
  positionId: any='';
  idNumber: any='';
  gender: any='';
  limit: any = 10;
  offset: any = 0;
  orderBy: any = 'id';
  sortType: any = 'ASC';

  dataEmp:any = [];
  responseCode:any;
  responseMessage:any;

  title='';

  ngOnInit() {
    this.getEmployeeData();
  }

  async getEmployeeData() {
    let body = {
      name: this.name,
      idNumber: this.idNumber,
      limit: this.limit,
      offset: this.offset,
      orderBy: this.orderBy,
      sortType: this.sortType
    }

    let data = await this.service.getEmployee(body).toPromise();
    this.dataEmp = data.data;
  }

  setDeleteId(id){
    this.id = id;
  }
  
  async deleteEmployee(){
    let body = {
      id: this.id
    }
    await this.service.deleteEmployee(body).toPromise();
    this.getEmployeeData();
  }

  addEmployee(){
    this.router.navigate(['/form']);
  }

  updateEmployee(id, data){
    let body = {
      objective: 'update',
      data: data
    }
    this.router.navigate([`/form`], {
      queryParams: {
        id
      },
      state: body
    })
  }

}
